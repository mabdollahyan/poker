#**POKer**
The Partial Order Kernel (POKer) is a convolution kernel for the comparison of strings containing alternative substrings. Each string is represented by a directed acyclic graph. POKer produces a measure of similarity between two strings by comparing their corresponding graphs. To do so, it uses dynamic programming to compute an exponentially weighted sum of the local alignment scores for all possible choices of paths in the graphs.

Please cite the below paper if you use this code:

- Abdollahyan M and Smeraldi F. POKer: a Partial Order Kernel for Comparing Strings with Alternative Substrings. In proceedings of the 25th European symposium on artificial neural networks, computational intelligence and machine learning (ESANN2017). Bruges, Belgium. 2017.

###**Requirements**
Compatible with Python 2.7 and 3. 
Requires NetworkX (1.11 or above) library.

###**Usage**
The code accepts input files in the following format: tab-delimited text file containing string ID, string name, and start and end positions of each substring within the string per line. The output is in the following format: tab-delimited text file containing reference sequence name, target sequence name and the value returned by the kernel per line. 

###**License**
Copyright (C) 2017 Maryam Abdollahyan and Fabrizio Smeraldi
