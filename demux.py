# Demultiplex sequences
# 
# Copyright (C) 2017 Maryam Abdollahyan and Fabrizio Smeraldi 

import sys
from operator import itemgetter
import networkx as nx


STARTNODE=('START:',-10,-1)  # Dummy node as the start to all paths

class Writer:
    pathsfile=None
    statsfile=None
    count=0
    overlaps=0
    seqname=''

    @staticmethod
    def open(pathsfilename, statsfilename):
        Writer.pathsfile=open(pathsfilename, 'w')
        Writer.statsfile=open(statsfilename, 'w')
        Writer.statsfile.write("NAME\tMOTIFS\tMERGED\tPATHS\n")

    @staticmethod
    def setseqname(name):
        Writer.seqname=name
        Writer.count=0
        Writer.overlaps=0

    @staticmethod
    def addoverlaps(olaps):
        Writer.overlaps+=olaps

    @staticmethod
    def writepath(path):
        Writer.count+=1
        Writer.pathsfile.write("%s-%d\n"%(Writer.seqname, Writer.count))
        Writer.pathsfile.write("%s\n"%path)

    @staticmethod
    def writestats(nmotifs):
        Writer.statsfile.write("%s\t%d\t%d\t%d\n"%(Writer.seqname, nmotifs, Writer.overlaps, Writer.count))

    @staticmethod
    def close():
        Writer.pathsfile.close()
        Writer.statsfile.close()


# Create edges. There is an edge between two nodes X and Y if start(Y) > end(X).
# There exits no node Z such that Z starts after end(X) and ends before start(Y)        
def compute_edges(G):
    # Sort motifs by start position
    motifs=sorted(G.nodes_iter(), key=itemgetter(1))
    
    for i in range(len(motifs)-1):
        startnode=motifs[i]
        j=i+1
        # Find the first non-overlapping motif
        while (motifs[j][1]<=startnode[2]): 
            j=j+1
            if j==len(motifs): break
        else:  # Unless we came out with a break
            leftmostend=motifs[j][2]
            # All target nodes must have a start position before the end position of motifs[j]...
            while motifs[j][1]<=leftmostend:
                if motifs[j][2]<=leftmostend:
                    leftmostend=motifs[j][2]  # or of any other target node
                G.add_edge(startnode, motifs[j])
                j=j+1
                if j==len(motifs): break

        
# Trace all paths starting from the given node, depth first. 
# Output the path when a leaf node is reached
def trace_paths(G, node, path):
    succs=G.successors(node)
    if len(succs)==0:
        Writer.writepath(path)
    else: 
        for nxt in succs:
            trace_paths(G, nxt, path+nxt[0])


# Merge the nodes in the given list: create a new node; link from the new node to
# all children of the nodes in the list; return the new node. List of identified 
# nodes is used in the name of the new node so that if same nodes are identified 
# twice as children of different parents, no new node is added but the first 
# identification is considered instead
def merge_children (G, nodes):
    sym=(nodes[0])[0]
    newnode=(sym, 0, tuple(sorted(nodes, key=itemgetter(1))))
    if newnode not in G.nodes_iter():
        G.add_node(newnode)
        Writer.addoverlaps(len(nodes))
    for n in nodes:
        children=G.successors(n)
        for c in children:
            G.add_edge(newnode, c)
    return newnode


# Remove duplicate paths
def remove_duplicates(G):
    done=False
    while not done:
        done=True
        nodes=G.nodes()
        for parent in nodes:
            # Keep into account the changes to the graph
            if parent not in G.nodes_iter(): continue
            identical=[]
            succs=sorted(G.successors(parent), key=itemgetter(0))
            if len(succs)<=1: continue  # Leaf or single child
            # Find the first group of children with equal symbols
            currsym=''
            succs.append(('dummy', 0, 0))
            i=0
            for j in range(0, len(succs)):
                sym=(succs[j])[0]
                if sym!=currsym:
                    if j-i>1:
                        identical=succs[i:j]
                        break
                    currsym=sym
                    i=j
            if len(identical):
                # Create a new node with children of identical nodes being merged
                newnode=merge_children(G, identical)
                # Link from parent to the new node
                G.add_edge(parent, newnode)
                # Unlink from parent to merged nodes. Remove if no other parents exist
                for child in identical:
                    G.remove_edge(parent, child)
                    if G.in_degree(child)==0:
                        G.remove_node(child)
                done=False
 
 
# Build a graph of the sequence of motifs
def build_graph(motifs):
    G=nx.DiGraph()
    startnode=STARTNODE
    G.add_node(startnode)
    G.add_nodes_from(motifs)
    compute_edges(G)
    remove_duplicates(G)
    return G


# Generate non-overlapping paths from the sequence of motifs
def demux(motifs):
    G=build_graph(motifs)
    startnode=STARTNODE
    trace_paths(G, startnode, '')
    
    
# Parse input file; demultiplex each sequence separately and output to paths file    
def process(infilename, pathsfilename, statsfilename):
    infile=open(infilename,'r')
    Writer.open(pathsfilename, statsfilename)

    next(infile)  # Skip the header
    seqname=''
    motifs=[]

    for line in infile:
        (name,symbol,start,stop)= line.expandtabs(1).split()[1:5]
        # New sequence; process the old one and reset
        if name!=seqname:
            if len(motifs):
                demux(motifs)
                Writer.writestats(len(motifs))
                motifs=[]
            seqname=name
            Writer.setseqname(seqname)
        # Current sequence; append the new motif as a tuple 
        motifs.append((symbol,int(start),int(stop)))
    # Process the last one
    demux(motifs)
    Writer.writestats(len(motifs))
        
    infile.close()
    Writer.close()


# Main
if __name__ == '__main__':
    if len(sys.argv)<3:
        print("Usage: demux.py <infile> <pathsfile> <statsfile>")
    else:
        process(*sys.argv[1:])
        