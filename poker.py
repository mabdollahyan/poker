# Apply dynamic programming to graph representations of sequences
# This version handles 'local' alignments
#
# Copyright (C) 2017 Maryam Abdollahyan and Fabrizio Smeraldi 

import sys
import math
import numpy as np
import networkx as nx
import demux as dmx


class Scores:
    start=0
    match=10
    mismatch=0
    gap=-1
    beta=0.5
    
class RefGraph:  # Reference graph, forward and reverse
    fwd=None
    rev=None


# Return a list of leaf nodes
def leaves(G):  
    return [n for n in G.nodes_iter() if G.out_degree(n)==0]


# Set score of nodes that have zero score
def setzeroscore(G):
    for node in G.nodes_iter():
            if ((node[0]==dmx.STARTNODE) or
                (node[1]==dmx.STARTNODE)):
                nx.set_node_attributes(G,'m', {node: Scores.start})
                nx.set_node_attributes(G,'n', {node: Scores.start})
                
                
# Score the edge between two nodes     
def edge_score(node): 
    if node[0][0]==node[1][0]:
        return Scores.match
    else:
        return Scores.mismatch 
    
    
# Dynamic programming on the product graph. Use a stack to iterate through graph
def dynprog(G):
    setzeroscore(G)              
    stack=leaves(G)
    while len(stack)>0:
        node=stack[-1]
        if 'm' in G.node[node]:  # Node has been processed so both 'm' and 'n' scores are available
            stack.pop()
            continue
        preds=G.predecessors(node)
        complete=True
        scores=[]
        nscores=[]
        prednscore=0
        pathscore=0
        for p in preds:
            # If the predecessor has not been processed yet add it to the queue
            if 'm' not in G.node[p]:  # ...then predecessor's 'n' score has not been computed yet
                stack.append(p)
                complete=False
            elif complete:  # ...else add its 'n' score
                if (p[0]!=node[0]) and (p[1]!=node[1]):  # Diagonal predecessor
                    pathscore=G.node[p]['n']
                    prednscore=-math.exp(2*Scores.beta*Scores.gap)*G.node[p]['n']
                else:  # Vertical/horizontal predecessor
                    pathscore=0    
                    prednscore=math.exp(Scores.beta*Scores.gap)*G.node[p]['n']
                scores.append(pathscore)
                nscores.append(prednscore)
        # If all predecessors are handled, compute 'm' and 'n' scores
        if complete:
            mscore=math.exp(Scores.beta*edge_score(node))*(1.0+sum(scores))
            nx.set_node_attributes(G,'m', {node: mscore})
            nscore=sum(nscores)+mscore
            nx.set_node_attributes(G, 'n', {node: nscore})  
            stack.pop()  # Node has been processed
    return G  


# Reverse the graph: reverse all edges and move the start node accordingly to point to
# leaves of forward graph
def reverse(G):
    H=G.copy()
    leafnodes=leaves(H)
    # List edges from start node and remove them
    H.remove_edges_from(H.edges([dmx.STARTNODE]))
    # Reverse edges in place
    H.reverse(copy=False)
    # Create edges from start node to former leaves
    for leaf in leafnodes:
        H.add_edge(dmx.STARTNODE, leaf)
    return H


# Compute reference graphs for alignment
def setreference(seq):
    RefGraph.fwd=dmx.build_graph(seq)
    RefGraph.rev=reverse(RefGraph.fwd)
    

# Use dynamic programming to align two sequences
def align(seqB):
    graphB=dmx.build_graph(seqB)   
    scores=[]
    for graphA in [RefGraph.fwd, RefGraph.rev]:
        score=0
        alnGraph=nx.strong_product(graphA, graphB)
        G=dynprog(alnGraph)
        for node in G.nodes_iter():  # Compute the sum of 'm' scores
            score=score+G.node[node]['m']
        scores.append(1.0+score)  # 'fwd' and 'rev' scores
    final_score=max(scores)
    return final_score  


# Read input
def readseqs(infilename, startline=0, startseq=''): 
    infile=open(infilename,'r')
    
    if startline>0:
        startseq=''
        for _ in range(0, startline):
            next(infile)
    endline=startline
    
    next(infile)  # Skip the header
    seqname=''
    motifs=[]
    
    for line in infile:
        (name,symbol,start,stop)= line.expandtabs(1).split()[1:5]
        # Discard all sequences before the start one
        if startseq!='':
            if name!=startseq:
                endline+=1
                continue
            else:
                startseq=''
        # New sequence; process the old one and reset
        if name!=seqname:
            if len(motifs):
                yield (seqname, motifs, endline)
                motifs=[]
            seqname=name
        # Current sequence; append the new motif as a tuple 
        motifs.append((symbol,int(start),int(stop)))
        endline+=1
    
    infile.close()
    yield (seqname, motifs, endline) 

 
def process (infilename, outfilename, startseq=''):
    outfile=open(outfilename, 'wt')
        
    endline=0  # End of previous reference
    
    while True:
        startline=endline  # Start from the next sequence as reference
        seqgenerator=readseqs(infilename, startline, startseq)
        seq1=next(seqgenerator)
        (name1, motifs1, endline)=seq1
        # Align seq1 with all remaining sequences
        setreference(motifs1)
        count=0
        score_self=align(motifs1)
        outfile.write("%s\t%s\t%f\n"%(name1, name1, score_self))
        for seq2 in seqgenerator:
            count+=1
            (name2, motifs2, dummy)=seq2                
            score=align(motifs2)
            outfile.write("%s\t%s\t%f\n"%(name1, name2, score))
        if count==0:  # Last one has been processed 
            break
        
    outfile.close()


# Main
if __name__ == '__main__':
    if len(sys.argv)<2:
        print ("Usage: poker.py <infile> <outfile> [<startseq> default: first one]")
    else:
        process(*sys.argv[1:])
